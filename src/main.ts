import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import App from './App.vue';
import About from './components/about.vue';
import BlogList from './components/blogList.vue';

const routes = [
    { path: '/about', component: About },
    { path: '/blog', component: BlogList },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

createApp(App).use(router).mount('#app');
